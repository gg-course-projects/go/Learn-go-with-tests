package main

import (
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {
	t.Run("Collection of 5 numbers", func(t *testing.T) {
		numbers := []int{1, 2, 3, 4, 5}

		result := sum(numbers)
		expected := 15

		if result != expected {
			t.Errorf("got %d, expected %d", result, expected)
		}
	})
}

func TestSumAll(t *testing.T) {
	got := sumAll([]int{1, 2}, []int{0, 9})
	expect := []int{3, 9}

	if !reflect.DeepEqual(got, expect) {
		t.Errorf("expected %v, got %v", expect, got)
	}
}

func TestSumAllTails(t *testing.T) {
	checkSums := func(t testing.TB, got, expect []int) {
		t.Helper()

		if !reflect.DeepEqual(got, expect) {
			t.Errorf("got %v, expected %v", got, expect)
		}
	}
	t.Run("make the sume of tail of slices", func(t *testing.T) {
		got := sumAllTails([]int{1, 2, 3}, []int{7, 8, 9, 10, 11})
		expect := []int{5, 38}

		checkSums(t, got, expect)
	})

	t.Run("Safely sum empty slice", func(t *testing.T) {
		got := sumAllTails([]int{}, []int{7, 8, 9, 10, 11})
		expect := []int{0, 38}

		checkSums(t, got, expect)
	})
}
