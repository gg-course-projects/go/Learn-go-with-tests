package main

func sum(numbers []int) int {
	sum := 0
	// for i := 0; i < len(numbers); i++ {
	for _, number := range numbers {
		sum += number
	}
	return sum
}

func sumAll(numbersToSum ...[]int) []int {
	var sums []int

	for _, numbers := range numbersToSum {
		sums = append(sums, sum(numbers))
	}
	return sums
}

func sumAllTails(numbersToSum ...[]int) []int {
	var summedTails []int

	for _, numbers := range numbersToSum {
		if len(numbers) == 0 {
			summedTails = append(summedTails, 0)
		} else {
			summedTails = append(summedTails, sum(numbers[1:]))
		}
	}
	return summedTails
}
