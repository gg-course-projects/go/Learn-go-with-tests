package dependency

import (
	"bytes"
	"fmt"
)

const englishHello = "Hello, "
const spanishHello = "Hola, "
const frenchHello = "Bonjour, "

//func Greet(name string) {
//	fmt.Printf(name)
//}

func Greet(writer *bytes.Buffer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}
