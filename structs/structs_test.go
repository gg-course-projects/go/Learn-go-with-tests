package main

import "testing"

//func TestPerimeter(t *testing.T) {
//	rectangle := Rectangle{2.5, 3}
//	got := Perimeter(rectangle)
//	expect := 11.0
//
//	assertTest(t, got, expect)
//
//}

func TestArea(t *testing.T) {

	//checkArea := func(t testing.TB, shape Shape, expect float64) {
	//	t.Helper()
	//	got := shape.Area() //	if got != expect { //		t.Errorf("got %g want %g", got, expect)
	//	}
	//}
	//t.Run("Test rectangle area", func(t *testing.T) {
	//	rectangle := Rectangle{2.5, 3}
	//	checkArea(t, rectangle, 7.5)
	//})

	//t.Run("Test circle area", func(t *testing.T) {
	//	circle := Circle{2}
	//	checkArea(t, circle, 12.566370614359172)
	//})

	areaTest := []struct {
		name  string
		shape Shape

		want float64
	}{
		{name: "Rectangle", shape: Rectangle{Width: 2.5, Height: 3}, want: 7.5},
		{name: "Circle", shape: Circle{Radius: 2}, want: 12.566370614359172},
		{name: "Triangle", shape: Triangle{Base: 12, Height: 6}, want: 36.0},
	}

	for _, tt := range areaTest {
		got := tt.shape.Area()
		if got != tt.want {
			t.Errorf("%#v got %g want %g", tt.shape, got, tt.want)
		}
	}
}
