package learnMaps

type Dictionary map[string]string

const (
	ErrWordNotFound      = DictionaryErr("expected to get an error")
	ErrWordExists        = DictionaryErr("word provided already exists in dictionary")
	ErrWordDoesNotExists = DictionaryErr("cannot update word because it does not exist")
)

type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

func (d Dictionary) search(word string) (string, error) {
	definition, ok := d[word]

	if !ok {
		return "", ErrWordNotFound
	}

	return definition, nil
}

func (d Dictionary) add(word, definition string) error {
	_, err := d.search(word)

	switch err {
	case ErrWordNotFound:
		d[word] = definition
	case nil:
		return ErrWordExists
	default:
		return err
	}
	return nil
}

func (d Dictionary) update(word, definition string) error {
	_, err := d.search(word)

	switch err {
	case ErrWordNotFound:
		return ErrWordDoesNotExists
	case nil:
		d[word] = definition
	default:
		return err
	}
	return nil
}

func (d Dictionary) delete(word string) {
	delete(d, word)
}
