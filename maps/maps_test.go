package learnMaps

import "testing"

func TestSearch(t *testing.T) {

	dictionary := Dictionary{"test": "this is just a test"}

	t.Run("Word exists", func(t *testing.T) {

		got, _ := dictionary.search("test")
		expect := "this is just a test"

		assertStrings(t, got, expect)
	})

	t.Run("Word does not exit", func(t *testing.T) {

		_, err := dictionary.search("nonExistent word")

		assertError(t, err, ErrWordNotFound)
	})

	t.Run("Add word to dictionary", func(t *testing.T) {
		word := "newWord"
		definition := "this is a new word"
		err := dictionary.add(word, definition)

		assertError(t, err, nil)
		assertDefinition(t, dictionary, word, definition)

	})

	t.Run("Add an existing word", func(t *testing.T) {
		word := "existingWord"
		definition := "this is an existing word"

		err := dictionary.add(word, definition)
		err = dictionary.add(word, definition)

		assertError(t, err, ErrWordExists)
	})
}

func TestUpdate(t *testing.T) {
	word := "wordToAdd"
	definition := "this is the word definition"
	dictionary := Dictionary{word: definition}
	newDefinition := "this is the new definition"

	t.Run("Test modifying new word", func(t *testing.T) {
		err := dictionary.update(word, newDefinition)
		assertError(t, err, nil)
		assertDefinition(t, dictionary, word, newDefinition)
	})

	t.Run("Test updating non exitent word", func(t *testing.T) {
		newWord := "newWord"
		err := dictionary.update(newWord, definition)

		assertError(t, err, ErrWordDoesNotExists)

	})

}

func TestDelete(t *testing.T) {
	word := "deletedWord"
	definition := "deleted word definitio"
	dictionary := Dictionary{word: definition}

	dictionary.delete(word)

	_, err := dictionary.search(word)
	if err != ErrWordNotFound {
		t.Errorf("Expected %q to be delted", word)
	}
}

func assertStrings(t testing.TB, got, expect string) {
	t.Helper()

	if got != expect {
		t.Errorf("got %s, expected %s", got, expect)
	}
}

func assertError(t testing.TB, got, expect error) {
	t.Helper()

	if got != expect {
		t.Errorf("got %q, expected %q", got, expect)
	}
}
func assertDefinition(t testing.TB, dictionary Dictionary, word, definition string) {
	got, err := dictionary.search(word)

	expect := definition
	if err != nil {
		t.Fatal("should have found word")
	}
	assertStrings(t, got, expect)
}
