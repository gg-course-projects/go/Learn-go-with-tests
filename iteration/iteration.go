package iteration

func Repeat(char rune, times int) string {
	repeated := string(char)
	for i := 1; i < times; i++ {
		repeated += string(char)
	}
	return repeated
}
